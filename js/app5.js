let alumnos = [
    {
        "matricula":"2021030314",
        "nombre":"Felipe Andrés Peñaloza Pizarro",
        "correo":"2021030314@upsin.edu.mx",
        "carrera":"Informática",
        "imagen":"../img/tidondeve.jpeg"
    }, 
    {
        "matricula":"2021030108",
        "nombre":"Fabio Manuel Noriega Fitch",
        "correo":"2021030108@upsin.edu.mx",
        "carrera":"Informática",
        "imagen":"../img/korreospuntokom.png"
    }, 
    {
        "matricula":"2021030077",
        "nombre":"Mateo Arias Tirado",
        "correo":"2021030077@upsin.edu.mx",
        "carrera":"Informática",
        "imagen":"../img/mateo3.jpeg"
    }, 
    {
        "matricula":"2015030311",
        "nombre":"Jonathan Alexis Reyes Lizárraga",
        "correo":"2015030311@upsin.edu.mx",
        "carrera":"Informática",
        "imagen":"../img/welo.jpeg"
    },
    {
        "matricula":"2021030142",
        "nombre":"Jorge Enrique García González",
        "correo":"2021030142@upsin.edu.mx",
        "carrera":"Informática",
        "imagen":"../img/peraka.png"
    },
    {
        "matricula":"2020030321",
        "nombre":"Yair Alejandro Ontiveros Govea",
        "correo":"2020030321@upsin.edu.mx",
        "carrera":"Informática",
        "imagen":"../img/sheeeet.png"
    },
    {
        "matricula":"2021030262",
        "nombre":"Ángel Ernesto Qui Mora",
        "correo":"2021030262@upsin.edu.mx",
        "carrera":"Informática",
        "imagen":"../img/goofy.jpeg"
    },
    {
        "matricula":"2021030136",
        "nombre":"Óscar Alejandro Solis Velarde ",
        "correo":"2021030136@upsin.edu.mx",
        "carrera":"Informática",
        "imagen":"../img/waawaa.png"
    },
    {
        "matricula":"2019030880",
        "nombre":"Julio Emiliano Quezada Ramos",
        "correo":"2019030880@upsin.edu.mx",
        "carrera":"Informática",
        "imagen":"../img/quezada.png"
    },
    {
        "matricula":"2021030135",
        "nombre":"Salvador Zepeda Zatarain",
        "correo":"2021030135@upsin.edu.mx",
        "carrera":"Nanotecnología",
        "imagen":"../img/salvaevil.png"
    },
];

const btnMostrar = document.getElementById('mostrar');
btnMostrar.addEventListener('click',function(){
    mostrar();
})
//Mostrar atributo de un arreglo del objeto
function mostrar(){
    const lista = document.getElementById("centro");

    alumnos.forEach((alumnos) => {
        const div = document.createElement("div");
        div.className = "alumnos";
        div.innerHTML = `
            <img src="${alumnos.imagen}">
            <p>Matrícula:<br>${alumnos.matricula}</p>
            <p>Nombre:<br>${alumnos.nombre}</p>
            <p>Carrera:<br>${alumnos.carrera}</p>
            <p>Correo:<br>${alumnos.correo}</p>
            <hr>
        `;
        lista.appendChild(div);
    });
    
    /*for(let  i=0; i<alumnos.length;i++){
        document.getElementById('matricula').innerHTML = alumnos[i].matricula;
        document.getElementById('nombre').innerHTML = alumnos[i].nombre;
        document.getElementById('correo').innerHTML = alumnos[i].correo;
        document.getElementById('carrera').innerHTML = alumnos[i].carrera;
        document.getElementById('imagen').src = alumnos[i].imagen;
    }*/
}