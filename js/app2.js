let valor = 0;
const btnCalcular = document.getElementById('btnCalcular');
btnCalcular.addEventListener('click',function(){
    //Obtener los datos de los inputs
    let pesoPersona = document.getElementById('pesoPersona').value;
    let alturaPersona = document.getElementById('alturaPersona').value;
    let edad = document.getElementById('edad').value;

    //Hacer los cálculos
    let alturaCuadrado = alturaPersona * alturaPersona;
    let imc = pesoPersona / alturaCuadrado;
    let imcr = imc.toFixed(2);
    let calM = 0;
    let calF = 0;
    if(edad > 9 && edad < 18){
        calM = 17.686 * pesoPersona + 658.2;
        calF = 13.384 * pesoPersona + 692.2;
    }

    if(edad > 17 && edad < 30){
        calM = 15.057 * pesoPersona + 692.2;
        calF = 14.818 * pesoPersona + 486.6;
    }

    if(edad > 30 && edad < 60){
        calM = 11.472 * pesoPersona + 873.1;
        calF = 8.126 * pesoPersona + 845.6;
    }

    if(edad >= 60){
        calM = 11.711 * pesoPersona + 587.7;
        calF = 9.082 * pesoPersona + 658.5;
    }
    document.getElementById('calM').value = calM.toFixed(2) + " - Hombre";
    document.getElementById('calF').value = calF.toFixed(2) + " - Mujer";
    
    //Mostrar los datos
    if(imcr < 18.5){
        valor = 1;
        cambiarImg(valor);
        document.getElementById('imc').value = imcr + " - Por debajo";
    }

    if(imcr > 18.5 && imcr <= 24.9){
        valor = 2;
        cambiarImg(valor);
        document.getElementById('imc').value = imcr + " - Saludable";
    }

    if(imcr > 25 && imcr <= 29.9){
        valor = 3;
        cambiarImg(valor);
        document.getElementById('imc').value = imcr + " - Sobrepeso";
    }

    if(imcr > 30 && imcr <= 34.9){
        valor = 4;
        cambiarImg(valor);
        document.getElementById('imc').value = imcr + " - Obesidad I";
    }

    if(imcr > 35 && imcr <= 39.9){
        valor = 5;
        cambiarImg(valor);
        document.getElementById('imc').value = imcr + " - Obesidad II";
    }
    
    if(imcr >= 40){
        valor = 6;
        cambiarImg(valor);
        document.getElementById('imc').value = imcr + " - Obesidad III";
    }
})

function cambiarImg(valor){
    if(valor == 1){
        document.getElementById('imagen2').src= "../img/01.png";
    }
    
    if(valor == 2){
        document.getElementById('imagen2').src= "../img/02.png";
    }

    if(valor == 3){
        document.getElementById('imagen2').src= "../img/03.png";
    }

    if(valor == 4){
        document.getElementById('imagen2').src= "../img/04.png";
    }

    if(valor == 5){
        document.getElementById('imagen2').src= "../img/05.png";
    }

    if(valor == 6){
        document.getElementById('imagen2').src= "../img/06.png";
    }
}
