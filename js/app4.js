//Obtener objeto button de calcular
//Declara objeto alumno

let alumno = {
    "matricula":"023030314",
    "nombre":"Josepo90YT",
    "grupo":"TI-73",
    "carrera":"Tecnologías de la Información",
    "foto":"/img/2023030314.png"
};

console.log("Matricula: " + alumno.matricula);
console.log("Nombre: " + alumno.nombre);

alumno.nombre = "Danirep";
console.log("Nuevo nombre: " + alumno.nombre);

//Objetos compuestos
let cuentaBanco = {
    "numero":"10001",
    "banco":"Banorte",
    cliente: {
        "nombre":"Jose Lopez",
        "fechaNac":"2020-01-01",
        "sexo":"M"
    },
    "saldo":"10000"
}

console.log("Nombre: " + cuentaBanco.cliente.nombre);
console.log("Saldo: " + cuentaBanco.saldo);
cuentaBanco.cliente.sexo = "F";
console.log("Nuevo sexo:" + cuentaBanco.cliente.sexo);

//Arreglo de productos

let productos = [
    {
        "codigo":"1001",
        "descripcion":"Atun",
        "precio":"34"
    }, 
    {
        "codigo":"1002",
        "descripcion":"Jabon de polvo",
        "precio":"23"
    }, 
    {
        "codigo":"1003",
        "descripcion":"Harina",
        "precio":"43"
    }, 
    {
        "codigo":"1004",
        "descripcion":"Pasta dental",
        "precio":"78"
    }
];

//Mostrar atributo de un arreglo del objeto
console.log("La descripción es: " + productos[0].descripcion);

for(let  i=0; i<productos.length;i++){
    console.log("Codigo: " + productos[i].codigo);
    console.log("Descripción: " + productos[i].descripcion);
    console.log("Precio: " + productos[i].precio);
}
