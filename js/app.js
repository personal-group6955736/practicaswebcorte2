/*Declarar variables*/

/* declarar variables */

const aside = document.getElementById('aside');
const btnCalcular = document.getElementById('btnCalcular');
btnCalcular.addEventListener('click',function(){
    //obtener los datos de los input
    let valorAuto = document.getElementById('valorAuto').value;
    let pIncial = document.getElementById('porcentaje').value;
    let plazos = document.getElementById('plazos').value;
    let parrafo = document.getElementById('pa');

    //hacer los calculos
    let pagoInicio = valorAuto * (pIncial/100);
    let totalFin = valorAuto-pagoInicio;
    let pagoMensual = totalFin/plazos;

    //mostrar los datos
    document.getElementById('pagoInicial').value = pagoInicio;
    document.getElementById('totalFin').value = totalFin;
    document.getElementById('pagoMensual').value = pagoMensual;

    let datos = document.createElement('span')
    datos.innerHTML=
    `
    Valor auto=$${valorAuto}
    <hr>
    <br>
    Porcentaje Inicial=$${pIncial}
    <hr>
    <br>
    Plazos=$${plazos}
    <hr>
    <br>
    Pagos Iniciales=$${pagoInicio}
    <hr>
    <br>
    Total a pagar=$${totalFin}
    <hr>
    <br>
    Pago Mensual=$${pagoMensual}
    <hr>
    Nuevo Registro
    <hr>
    `;
    aside.appendChild(datos);
})

function arribaMouse(){
    parrafo = document.getElementById('pa');
    parrafo.style.color ="#FF00FF"
    parrafo.style.fontSize = '25px';
    parrafo.style.transition = '1.0s'
    parrafo.style.textAlign = 'justify'
}

function salirMouse(){
    parrafo = document.getElementById('pa');
    parrafo.style.color = 'red';
    parrafo.style.fontSize = '17px';
    parrafo.style.textAlign = 'left'
}

function Limpiar(){
    let parrafo = document.getElementById("pa");
    parrafo.innerHTML="";
}

function Limpiar2(){
    document.getElementById('pagoInicial').value = "";
    document.getElementById('totalFin').value = "";
    document.getElementById('pagoMensual').value = "";
    document.getElementById('valorAuto').value = "";
    document.getElementById('porcentaje').value = "";
    document.getElementById('plazos').value = "";
}

