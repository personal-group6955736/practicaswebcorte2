/* Manejo de arrays */
//Declaración de array con elementos enteros
let arreglo2 = [];
arreglo2.length = 1;
//Diseñar una función que recibe como argumento un arreglo de enteros e imprime cada elemento y el tamaño del arreglo

/*function mostrarArray(arreglo){
    let tamaño = arreglo.length;

    for(let con=0; con < tamaño; ++ con){
        console.log(con + ":" + arreglo[con]);
    }

    console.log("Tamaño :" + tamaño);
}*/
//console.log(mostrarArray(arreglo));

//Función para mostrar el promedio de los elementos de array
function mostrarProm(arreglo2){
    let res = 0;
    let prom = 0;
    for(let i = 0; i < arreglo2.length; i++){
        res = res + parseInt(arreglo2[i]);
    }
    
    prom = res/arreglo2.length;
    console.log(res);
    console.log(arreglo2.length);
    console.log(prom);
    return prom;
}
//console.log(mostrarProm(arreglo));
//Función para mostrar los valores pares de un arreglo
function mostrarPares(arreglo2){
    let pares = 0;
    for(let con=0; con < arreglo2.length; con++){
        if(arreglo2[con]%2 == 0){
            pares++;
        }
    }
    return pares;
}

function mostrarImpares(arreglo2){
    let impares = 0;
    for(let con=0; con < arreglo2.length; con++){
        if(arreglo2[con]%2 != 0){
            impares++;
        }
    }
    return impares;
}
//console.log(mostrarPares(arreglo));
//Función para mostrar el valor mayor de los elementos de un arreglo
function mostrarMayor(arreglo2){
    let mayor = 0;
    let arreglo3 = arreglo2.slice(); 
    for (let i = 0; i < arreglo3.length; i++) {
        if(arreglo3[i] >= arreglo3[i+1]){
            mayor = arreglo3[i];
            arreglo3[i+1] = mayor;
        }
        console.log(mayor);
    }

    arreglo2 = arreglo3; 
    console.log("Mayor: " + mayor);
    return mayor;
}
//console.log(mostrarMayor(arreglo));

//Función para llenar con valores aleatorios el arreglo
function randomVar(arreglo2){
    for(let con=0; con < arreglo2.length; con++){
        arreglo2[con] = (Math.random() * 50).toFixed(0);
        console.log(con + ":" + arreglo2[con]);
    }
}
//console.log(randomVar(arreglo2));

//Función que muestra el valor menor y la posición del arreglo
function mostrarMenor(arreglo2) {
    let arreglo3 = arreglo2.slice();
    let min = arreglo3[0];
    for(let i = 1; i < arreglo3.length; i++){
       if(min <= arreglo3[i]){
        console.log("Primer valor: " + arreglo3[i]);
        console.log("Segundo valor: " + arreglo3[i+1]);
        console.log("Minimo: " + min);
        min = arreglo3[i];
        console.log("Postminimo: " + min);
        console.log("--------------");
       }
   }
   return min;
}
//console.log(mostrarMenor(arreglo));

//Función para comprobar si el generador de números aleatorios es simétrico (que la diferencia de números pares 
//e impares no sea mayor al 20%)

function simetriaP(arreglo2){
    let par = 0;
    let tamaño = arreglo2.length;
    for(let con=0; con < tamaño; con++){
        if(arreglo2[con]%2 == 0){
            par++;
        }
    }
    return par *= 100 / tamaño;
}

function simetriaI(arreglo2){
    let impar = 0;
    let tamaño = arreglo2.length;
    for(let con=0; con < tamaño; con++){
        if(arreglo2[con]%2 != 0){
            impar++;
        }
    }
    return impar *= 100 / tamaño;
}

function randomSimetrico(arreglo2){
    let par = 0, impar = 0;
    let simetrico = 0;
    let tamaño = arreglo2.length;
    for(let con=0; con < tamaño; con++){
        if(arreglo2[con]%2 == 0){
            par++;
        }
        else{
            impar++;
        }
    }
    console.log("Pares: " + par);
    console.log("Impares: " + impar);
    if(par > impar){
        simetrico = par - impar;
        simetrico*=100/tamaño;
        console.log("Simetria: " + simetrico);
    }
    else if(impar > par){
        simetrico = impar - par;
        simetrico*=100/tamaño;
        console.log("Simetria: " + simetrico);
    }
    for(let con=0; con < tamaño; ++ con){
        console.log(con + ":" + arreglo2[con]);
    }
    return simetrico;

}
//console.log(randomSimetrico(arreglo2));

const btnCapturar = document.getElementById('btnCapturar');
btnCapturar.addEventListener('click',function(){
    tamañoArr = document.getElementById('tamañoArreglo').value;
    arreglo2.length = tamañoArr;
    randomVar(arreglo2);
})

const btnCalcular = document.getElementById('btnCalcular');
btnCalcular.addEventListener('click',function(){
    //document.getElementById('promedio').value = console.log(mostrarPares(arreglo))

    
    let promedio = mostrarProm(arreglo2);
    document.getElementById('promedio').value = promedio;

    /*let menor = mostrarMenor(arreglo2);
    document.getElementById('menor').value = menor;

    let mayor = mostrarMayor(arreglo2);
    document.getElementById('mayor').value = mayor;*/

    let pares = mostrarPares(arreglo2);
    document.getElementById('pares').value = pares;

    let impares = mostrarImpares(arreglo2);
    document.getElementById('impares').value = impares;

    let simetriaPar = simetriaP(arreglo2);
    document.getElementById('simetriaPar').value = simetriaPar + "%";

    let simetriaImpar = simetriaI(arreglo2);
    document.getElementById('simetriaImpar').value = simetriaImpar + "%";

    let simetria = randomSimetrico(arreglo2);
    if (simetria <= 20){
        document.getElementById('simetria').value = simetria + "% margen de error - - SIMETRICO";
    }
    else{
        document.getElementById('simetria').value = simetria + "% margen de error - - NO SIMETRICO";
    }
})

const comboBX = document.getElementById('comboBX');
comboBX.addEventListener('click',function(){
    for(let i = 0; i < arreglo2.length; i++){
        let option = document.createElement('option');
        option.text = arreglo2[i];
        comboBX.remove(option); 
    }
    for(let i = 0; i < arreglo2.length; i++){
        let option = document.createElement('option');
        option.text = arreglo2[i];
        comboBX.add(option);
    }
})

